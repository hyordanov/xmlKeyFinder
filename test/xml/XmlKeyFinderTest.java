package xml;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class XmlKeyFinderTest {

	private final String contentFile = "<?xml version=\"1.0\" encoding=\"utf-8\"?><crc xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" versao=\"1.0\"><controlo entObserv=\"0019\" entReport=\"0019\" idDest=\"0019\" dtCriacao=\"2018-04-04T20:30:12\" /><conteudo><dissCorr dtRefDiss=\"2017-09-30\"><respEnt dissSI=\"S14\" dtRefDiss=\"2017-09-30\" numPart=\"1\"";

	private File tmp = null;

	@Before
	public void setUp() throws IOException {
		if (tmp == null) {
			tmp = Files.createTempFile("testXmlKeyFinder", "xml").toFile();
			tmp.deleteOnExit();
			Files.write(Paths.get(tmp.getAbsolutePath()), contentFile.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
		}
	}

	@After
	public void tearDown() {
		if (tmp != null)
			tmp.delete();
	}

	@Test
	public void testEmptyArgs() {
		XmlKeyFinder.main(buildArgs());
	}

	@Test
	public void testMore2Args() {
		XmlKeyFinder.main(buildArgs("abcd", "abcdef", "asadsd"));
	}

	@Test
	public void testFileNotExists() {
		XmlKeyFinder.main(buildArgs("abcd", "abcdef"));
	}

	@Test
	public void testInvalidFile() {
		XmlKeyFinder.main(buildArgs("C:\\Windows\\", "abcdef"));
	}

	@Test
	public void testInvalidEmptyKey() {
		XmlKeyFinder.main(buildArgs(tmp.getAbsolutePath(), " "));
	}

	@Test
	public void testInvalidNullKey() {
		XmlKeyFinder.main(buildArgs(tmp.getAbsolutePath(), null));
	}

	@Test
	public void testOk() {
		XmlKeyFinder.main(buildArgs(tmp.getAbsolutePath(), "dtRefDiss"));
	}

	private String[] buildArgs(String... args) {
		String[] result = new String[args.length];
		for (int i = 0; i < args.length; i++)
			result[i] = args[i];
		return result;
	}

}
