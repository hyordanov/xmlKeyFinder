package xml.utils;

import java.util.Scanner;

public class ScannerUtils {
	private static Scanner in = null;

	static {
		try {
			in = new Scanner(System.in);
		} catch (Exception e) {
			fatalException(e);
			Logger.error("FATAL ERROR Was not possible to iniatilize System Scanner. " + e.getMessage());
			System.exit(-1);
		}
	}

	public static int readInt() {
		boolean valid = false;
		int option = -1;
		while (!valid) {
			try {
				option = in.nextInt();
				valid = true;
			} catch (Exception e) {
				fatalException(e);
				in.nextLine();
			}
		}
		in.reset();
		return option;
	}

	public static String readString() {
		boolean valid = false;
		String option = "";
		while (!valid) {
			try {
				option = in.nextLine();
				if (option != null && !option.trim().isEmpty())
					valid = true;
			} catch (Exception e) {
				fatalException(e);
				in.nextLine();
			}
		}
		in.reset();
		return option;
	}

	public static void pressAnyKeyToContinue() {
		Logger.info("");
		Logger.info("Press any key to continue...");
		try {
			System.in.read();
		} catch (Exception e) {
			fatalException(e);
		}
	}

	public static void fatalException(Exception e) {
		if (e == null)
			return;
		if ("No line found".equals(e.getMessage())) {
			Logger.errorFatal(
					"System Scanner not found. Execute program in command line (Win) or terminal (unix) to skip this error.");
			System.exit(-1);
		}
	}
}
