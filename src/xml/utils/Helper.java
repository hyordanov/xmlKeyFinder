package xml.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import xml.key.Key;

public class Helper {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	public static final String nl = "\n";

	public static void printWelcome() {
		StringBuilder sb = new StringBuilder();
		sb.append(" __  __  __  __   _         _____   ___   _   _   ____    _____   ____  ").append(nl);
		sb.append(" \\ \\/ / |  \\/  | | |       |  ___| |_ _| | \\ | | |  _ \\  | ____| |  _ \\ ").append(nl);
		sb.append("  \\  /  | |\\/| | | |       | |_     | |  |  \\| | | | | | |  _|   | |_) |").append(nl);
		sb.append("  /  \\  | |  | | | |___    |  _|    | |  | |\\  | | |_| | | |___  |  _ < ").append(nl);
		sb.append(" /_/\\_\\ |_|  |_| |_____|   |_|     |___| |_| \\_| |____/  |_____| |_| \\_\\").append(nl);
		sb.append("                                                           by: hyordanov").append(nl);
		sb.append(nl);
		Logger.info(sb.toString());
	}

	public static void printOptionSelected(int option) {
		Helper.clearConsole();
		Helper.printWelcome();
		switch (option) {
		case 1:
			Logger.info("SEARCH A KEY IN A EXISTING FILE");
			break;
		case 9:
			Logger.info("HELP");
			break;

		default:
			break;
		}
		Logger.info("");
	}

	public static void printOptions() {
		StringBuilder sb = new StringBuilder();
		sb.append("*** MENU ***").append(nl);
		sb.append("1: Search a key in a existing file").append(nl);
		sb.append("9: Help").append(nl);
		sb.append("0: Exit").append(nl);
		Logger.info(sb.toString());
	}

	public static void printMenu() {
		Helper.clearConsole();
		Helper.printWelcome();
		Helper.printOptions();
	}

	public static int readOption() {
		boolean valid = false;
		int option = -1;
		int tries = 0;
		while (!valid) {
			try {
				Logger.infoSameLine("Option: ");
				option = ScannerUtils.readInt();
				valid = true;
				if (tries > 3) {
					Logger.error("You selected invalid option more 3 times");
					return 0;
				}
			} catch (Exception e) {
				ScannerUtils.fatalException(e);
				Logger.error("Option selected is not valid, try again");
				tries++;
			}
		}
		Logger.info("");
		return option;
	}

	public static void printEnd() {
		Logger.info(nl + "*** END ***");
	}

	public static void printInvalidSizeArgs() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Arguments:").append(nl);
		sb.append("  1) Absolute path to XML file").append(nl);
		sb.append("  2) Key (case sensitive) to find").append(nl);
		Logger.info(nl + sb.toString());
	}

	public static void printHelp() {
		final StringBuilder sb = new StringBuilder();
		sb.append("This program will help you to find a KEY (not tag) in XML file").append(nl);
		sb.append(nl).append("Requisites:").append(nl);
		sb.append("  - Java 1.8 installed");
		Logger.info(sb.toString());
		printInvalidSizeArgs();
	}

	public static File processKeysToFile(List<Key> keys, File xmlFile, String key) {
		List<String> data = new ArrayList<>();
		data.add("***********************");
		data.add("*** XML KEY FINDER ***");
		data.add("***********************");
		data.add("");
		data.add("PARAMS");
		data.add("------");
		data.add("File: " + xmlFile.getAbsolutePath());
		data.add(" Key: " + key);
		data.add("" + nl);
		data.add("TOTAL");
		data.add("-----");
		if (keys.size() != 1)
			data.add("Found " + keys.size() + " unique keys");
		else
			data.add("Found " + keys.size() + " unique key");
		data.add("" + nl);
		data.add("RESULT");
		data.add("------");
		for (int i = 0; i < keys.size(); i++) {
			data.add(keys.get(i).toString(i + 1));
		}
		data.add("");
		final File f = new File("XmlKeyFinder_Output" + sdf.format(new Date()) + ".txt");
		if (f.exists())
			f.delete();
		try {
			f.createNewFile();
			Files.write(Paths.get(f.getAbsolutePath()), data);
		} catch (IOException e) {
			Logger.error("Error occurred on try create/write to  a new file: " + f.getAbsolutePath());
			e.printStackTrace();
		}

		return f;
	}

	public final static void clearConsole() {
		try {
			final String os = System.getProperty("os.name");

			if (os.contains("Windows")) {
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			} else {
				Runtime.getRuntime().exec("clear");
			}
		} catch (final Exception e) {
			for (int i = 0; i < 80; i++)
				System.out.println("");
		}
	}
}
