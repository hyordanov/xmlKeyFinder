package xml.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	private static File log = null;

	static {
		log = new File("XmlKeyFinder.log");
		if (!log.exists())
			try {
				log.createNewFile();
			} catch (IOException e) {
				System.err.println("Error occurred on try to create a log file");
				e.printStackTrace();
			}
	}

	public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:SSS");

	public static synchronized void infoSameLine(String msg) {
		if (msg == null)
			msg = "";
		for (String m : formmatedMsg(msg)) {
			final String fm = SDF.format(new Date()) + " INFO - " + m;
			System.out.print(fm);
			writeToLog(fm);
		}
	}

	public static synchronized void info(String msg) {
		if (msg == null)
			msg = "";
		for (String m : formmatedMsg(msg)) {
			final String fm = SDF.format(new Date()) + " INFO - " + m;
			System.out.println(fm);
			writeToLog(fm);
		}
	}

	public static synchronized void error(String msg) {
		if (msg == null)
			msg = "";
		for (String m : formmatedMsg(msg)) {
			final String fm = SDF.format(new Date()) + " ERROR - " + m;
			System.err.println(fm);
			writeToLog(fm);
		}

	}
	
	public static synchronized void errorFatal(String msg) {
		if (msg == null)
			msg = "";
		for (String m : formmatedMsg(msg)) {
			final String fm = SDF.format(new Date()) + " FATAL ERROR - " + m;
			System.err.println(fm);
			writeToLog(fm);
		}

	}

	private static String[] formmatedMsg(final String msg) {
		return msg.split("\n");
	}

	private static synchronized void writeToLog(String msg) {
		if (log != null) {
			try {
				msg += "\n";
				Files.write(Paths.get(log.getAbsolutePath()), msg.getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
			}
		}
	}
}
