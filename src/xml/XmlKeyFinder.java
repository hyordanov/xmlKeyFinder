package xml;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import xml.key.Key;
import xml.key.finder.Finder;
import xml.utils.Helper;
import xml.utils.Logger;
import xml.utils.ScannerUtils;

public class XmlKeyFinder {
	public static void main(String[] args) {
		if (args.length > 0) {
			Helper.printWelcome();
			if (args.length == 2) {
				if (validate(args[0], args[1]))
					search(args[0], args[1]);
			} else {
				Logger.error("Number of arguments received is not correct");
				Helper.printInvalidSizeArgs();
			}
			return;
		}

		int option = 0;
		do {
			Helper.printMenu();
			Logger.info("");
			option = Helper.readOption();
		} while (executeOption(option));

		Helper.printEnd();
	}

	private static boolean executeOption(int option) {
		if (option == 0)
			return false;

		Helper.printOptionSelected(option);
		if (option == 1) {
			String pathFile = null;
			String key = null;
			Helper.printOptionSelected(option);
			Logger.infoSameLine("Absolute path to XML file: ");
			pathFile = ScannerUtils.readString();
			Logger.infoSameLine("Key: ");
			key = ScannerUtils.readString();
			Logger.info("");
			if (validate(pathFile, key))
				search(pathFile, key);
			ScannerUtils.pressAnyKeyToContinue();

		} else if (option == 9) {
			Helper.printHelp();
			ScannerUtils.pressAnyKeyToContinue();
		}
		return true;
	}

	private static boolean validate(String path, String key) {
		Logger.info("Validating XML file");
		if (path == null || path.trim().isEmpty()) {
			Logger.error("Absolute path to XML file is not valid. Cannot be null and empty");
			return false;
		}
		final File xmlFile = new File(path);
		if (!xmlFile.exists()) {
			Logger.error("XML file '" + xmlFile.getAbsolutePath() + "' not exists");
			return false;
		} else if (!xmlFile.isFile()) {
			Logger.error("XML file '" + xmlFile.getAbsolutePath() + "' is not valid");
			return false;
		}

		Logger.info("Validating key");
		if (key == null || key.trim().isEmpty()) {
			Logger.error("Key is not valid. Cannot be null and empty");
			return false;
		}
		return true;
	}

	private static void search(String pathFile, String key) {
		final File xmlFile = new File(pathFile);
		Logger.info(Helper.nl + "Searching all '" + key + "' in file: " + xmlFile.getName() + "... please wait");
		final Finder finder = new Finder(xmlFile, key);
		finder.start();
		finder.wait4Finish();
		List<Key> keys = finder.getKeys();
		if (keys.size() != 1) {
			Logger.info(Helper.nl + "Found " + keys.size() + " keys");
			if (keys.size() > 1) {
				Logger.info("Removing repeated...");
				Set<Key> hs = new HashSet<>();
				hs.addAll(keys);
				keys.clear();
				keys.addAll(hs);
			}
		} else
			Logger.info(Helper.nl + "Found " + keys.size() + " key");

		Logger.info("Processing them to a new file...");
		final File outputFile = Helper.processKeysToFile(keys, xmlFile, key);
		if (outputFile.length() > 0) {
			Logger.info("Opening result file with default program...");
			try {
				Desktop.getDesktop().open(outputFile);
			} catch (IOException e) {
				Logger.error("Error occurred on try to open file " + outputFile.getAbsolutePath());
				e.printStackTrace();
			}
		}
	}

}
