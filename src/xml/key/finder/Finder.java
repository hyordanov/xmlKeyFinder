package xml.key.finder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import xml.key.Key;
import xml.utils.Logger;

public final class Finder {
	private final int BUFF_SIZE = 20;
	private long readCount = 0;

	private final long totalReads;

	private final File xmlFile;

	private final List<Key> keys;
	private final Pattern pattern;

	private CountDownLatch mutex = null;

	public Finder(final File xmlFile, final String key) {
		this.xmlFile = xmlFile;
		this.keys = new ArrayList<>();
		this.mutex = new CountDownLatch(1);
		long tmpTotalReads = xmlFile.length() / BUFF_SIZE;
		if (tmpTotalReads == 0)
			tmpTotalReads = 1;
		this.totalReads = tmpTotalReads;
		this.pattern = Pattern.compile("(\\S*" + key + "=\"\\S+\")");
	}

	public synchronized void start() {
		if (this.readCount > 0) {
			Logger.error("Cannot start finder, already started before!");
			return;
		}

		final Thread t = new Thread(new Runnable() {
			private void printProcess() {
				final float process = (readCount * 100) / totalReads;
				Logger.info(String.format("%1.2f", process) + " %");
			}

			@Override
			public void run() {
				final Timer tprocess = new Timer();
				tprocess.schedule(new TimerTask() {
					@Override
					public void run() {
						printProcess();
					}
				}, new Date(), 1000); // every second

				try (BufferedReader reader = new BufferedReader(new FileReader(xmlFile));) {
					StringBuilder data = new StringBuilder();
					char[] buffer = new char[BUFF_SIZE];
					int read = 0;
					while ((read = reader.read(buffer)) != -1) {
						data.append(String.valueOf(buffer, 0, read));

						if (data.toString().endsWith(" ")) {
							buffer = new char[BUFF_SIZE];
							readCount++;
							extractKey(data);
							data.setLength(0);
						} else
							buffer = new char[1];
					}
					reader.close();

					extractKey(data);
					data.setLength(0);

					readCount = totalReads;
					printProcess();

				} catch (FileNotFoundException e) {
					Logger.error("XML File is not found");
					e.printStackTrace();
				} catch (IOException e) {
					Logger.error("IOException occurred on finder");
					e.printStackTrace();
				}

				tprocess.cancel();
				mutex.countDown();
			}
		});

		t.start();
	}

	public void wait4Finish() {
		try {
			this.mutex.await();
		} catch (Exception e) {
			Logger.error("Error ocurred on try to wait for finder to end " + e.getMessage());
		}
	}

	private void extractKey(final StringBuilder data) {
		if (data == null || data.toString().trim().isEmpty())
			return;

		final Matcher m = this.pattern.matcher(data);
		while (m.find()) {
			final String kv = m.group();
			final String[] kvArr = kv.split("=");
			final Key key = new Key(kvArr[0], kvArr[1].replaceAll("\"", ""));
			this.keys.add(key);
		}

	}

	public List<Key> getKeys() {
		return this.keys;
	}
}
