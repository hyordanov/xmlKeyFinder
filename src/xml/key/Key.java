package xml.key;

import java.util.stream.IntStream;

public class Key {
	private final String key;
	private final String value;

	public Key(final String key, final String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.key).append("=\"").append(this.value).append("\"");
		return sb.toString();
	}

	public String toString(int nrLine) {
		final String orginal = this.toString();
		StringBuilder sb = new StringBuilder();
		sb.append(nrLine).append("# ");
		sb.append(orginal);
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Key) {
			final Key k = (Key) obj;
			return k.key.equals(this.key) && k.value.equals(this.value);
		} else
			return false;
	}

	@Override
	public int hashCode() {
		final String result = this.key + this.value;
		int[] hash = new int[result.length()];
		for (int i = 0; i < hash.length; i++)
			hash[i] = result.getBytes()[i];
		int sum = IntStream.of(hash).sum();
		return sum;
	}

}
