# XML Key Finder
Ultra optimized key search in XML files.

Works with Java 1.8 or higher.

Tested in Windows 7 with 15Mb (more than 30 000 keys) XML file.
